with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO;

package body amerique is

   function Get_Duree return tduree is
      duree_i : integer;
   begin
      Put_line("Jours ?");
      Ada.Integer_Text_IO.Get(duree_i);
      return tduree(duree_i);
   end Get_Duree;

   procedure Faire_Canada(avion : tprix := 1000.0 ; hotel : tprix := 100.0) is

      -- voyages au Canada : -20% sur 860e d'avion et 7j � 48e/j.
      type tpromo is digits 3 range 0.0 .. 100.0;
      promo : tpromo := 20.0; -- appliqu� � l'ensemble du prix

      duree : tduree := 7;
      total : tprix;
      type tjours is (lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche);
      type tweekend is (samedi, dimanche);
      type tjsemaine is new tjours range lundi .. vendredi;
      subtype tgrandweekend is tjours range vendredi .. dimanche;
      subtype tgdweekend is tgrandweekend;
      depart : tjours := samedi;
      arrivee : tgrandweekend := dimanche;
      voldenuit : boolean := depart /= arrivee;
      infos : string := "Voyages au Canada : -" & promo'Image &
        " % sur " & avion'Image & "e d'avion et " &
        duree'Image & "jours a " & hotel'Image & "e";
      infostotal : string (1 .. 28);
      voyage_moyen:boolean := duree > 3 and then duree < 10;
      --   type ttkm is array(tduree) of integer;
      --   tkm : ttkm;
      type ttkm is array(tduree range <>) of integer;
      tkm : ttkm (tduree);
      -- type t2dims is array(1..4, 2..3) of float;
   begin
      Ada.Text_IO.Put_line("Bienvenue dans l'agence de voyage");
      duree := get_duree;
      Ada.Text_IO.Put_line(infos);
      -- corriger : calculer le total complet
      total := tprix(float(hotel) * float(duree) + float(avion));
      total := total * tprix(1.0 - float(promo) / 100.0);
      infostotal := "Total Canada : " & total'Image & "e";
      Ada.Text_IO.Put_line(infostotal);
      Ada.Text_IO.Put_line("Depart le : " & depart'Image & ".");
      Ada.Text_IO.Put_line("Depart dimanche ? " & boolean(depart=dimanche)'Image & ".");
      -- En g�n�ral : Air Canada ;
      -- Voyages court (<7) : Quebec'Air
      -- Voyage de 7, 14, 21, 28j : Can'jet
      case duree is
      when 0 .. 6  =>  Ada.Text_IO.Put_line("Quebec'Air");
      when 7 | 14 | 21 | 28 =>  Ada.Text_IO.Put_line("Can'Jet");
      when others =>  Ada.Text_IO.Put_line("Air Canada");
      end case;
      -- Ada 2012+ seulement :
      Ada.Text_IO.Put_line( if duree > 45 then "Visa !" else "" );

      --   type ttkm is array(tduree) of integer;
      --   tkm : ttkm;
      --   totkm : integer;
      --    tkm(1 .. 2) := (0, 0);
      for j in 1 .. duree loop
         if j = 1 or j = duree then
            Ada.Text_IO.Put_line("Jour" & j'Image & ": avion");
            tkm(j) := 6500;
         elsif j mod 4 = 1 then
            Ada.Text_IO.Put_line("Jour" & j'Image & ": repos");
            tkm(j) := 0;
         else
            Ada.Text_IO.Put_line("Jour" & j'Image & ": marche");
            tkm(j) := 22;
         end if;
      end loop;
      -- refaire une boucle pour compter le total de km, et afficher

      declare
         totkm : integer;
      begin
         totkm := 0;
         for j in 1 .. duree loop
            totkm := totkm + tkm(j);
         end loop;
         Ada.Text_IO.Put_line("Total : " & totkm'Image & "km");
      end;

   end faire_canada;

end Amerique;
