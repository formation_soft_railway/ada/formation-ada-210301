with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Exceptions;

package body afrique is

   -- activation des vérifications :
   -- edit>project props. > build > Ada > Enable Assertions
   function Get_Prix_Namibie(j:positive) return positive
   with pre => j>1 and j<50
   is
   begin
      return j*37+1210;
   end Get_Prix_Namibie;

   procedure faire_namibie is
      j:positive;
      ex_que7:Exception;
   begin
      Put_Line("Namibie ; jours ? ");
      Get(j);
      if j mod 7 /= 0 then
         Ada.Exceptions.Raise_Exception(ex_que7'Identity, "Avion que le samedi");
         -- ou
         raise ex_que7 with "Avion que le samedi";
      end if;
      Put_Line(j'Image & "jours enregistre");
      Put_Line("Prix TTC : " & Get_Prix_Namibie(j)'Image & "e");
   exception
      when e:ex_que7 => Put_Line("Exception - " &
                                    Ada.Exceptions.Exception_Message(e));
      when e:others => Put_Line("Exception others - " &
                                    Ada.Exceptions.Exception_Message(e));
   end Faire_namibie;
end Afrique;
