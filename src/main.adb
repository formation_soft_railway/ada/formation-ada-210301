-- pragma Profile(Ravenscar);

with Amerique; use Amerique;
with Europe; use Europe;
with Afrique; use Afrique;
with Asie; use Asie;
with Oceanie; use Oceanie;
with Ada.Text_IO;

procedure Main is

begin
   -- Faire_canada(860.0, 48.0);
   -- Faire_canada(860.0);
   -- Faire_canada;
   -- Faire_canada(hotel => 48.0);
   -- Faire_Espagne;
   -- Faire_Italie;
   --Faire_Namibie;
   -- Faire_Russie;
   -- Faire_Japon;
   Faire_Australie;
   Ada.Text_IO.Put_Line("Au revoir !");
end Main;
